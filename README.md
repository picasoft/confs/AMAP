Source de la conférence "Les AMAP du numérique", présentée par les associations Rhizome et Picasoft pendant la Semaine Étudiante de la Durabilité, à l'UTC, au printemps 2019.

La vidéo de la conférence est disponible [sur Peertube](https://tube.aquilenet.fr/videos/watch/31072a55-90dd-4476-88f4-48cb57036f6d).
